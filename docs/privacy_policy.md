# Privacy Policy

Personal data (usually referred to just as "data" below) will only be processed by us to the extent necessary and for the purpose of providing a functional and user-friendly website, including its contents, and the services offered there.

Per Art. 4 No. 1 of Regulation (EU) 2016/679, i.e. the General Data Protection Regulation (hereinafter referred to as the "GDPR"), "processing" refers to any operation or set of operations such as collection, recording, organization, structuring, storage, adaptation, alteration, retrieval, consultation, use, disclosure by transmission, dissemination, or otherwise making available, alignment, or combination, restriction, erasure, or destruction performed on personal data, whether by automated means or not.

The following privacy policy is intended to inform you in particular about the type, scope, purpose, duration, and legal basis for the processing of such data either under our own control or in conjunction with others. We also inform you below about the third-party components we use to optimize our website and improve the user experience which may result in said third parties also processing data they collect and control.

Our privacy policy is structured as follows:

1. Information about us as controllers of your data
2. The rights of users and data subjects
3. Information about the data processing

## 1. Information about us as controllers of your data

The party responsible for this website (the "controller") for purposes of data protection law is:

Kraut und Rüben GmbH \
Example street 1 \
20097 Hamburg \
Germany \
Telephone: +49 40 1122 3344 \
Fax: +49 40 1122 3340 \
Email: info@krautundrueben.example.com

The controller's data protection officer is:

Maxie Musterfrau \
Example street 1 \
20097 Hamburg \
Germany \
Telephone: +49 40 1122 3355 \
Fax: +49 40 1122 3356 \
Email: datenschutz@krautundrueben.example.com

## II. The rights of users and data subjects

With regard to the data processing to be described in more detail below, users and data subjects have the right

- to confirmation of whether data concerning them is being processed, information about the data being processed, further information about the nature of the data processing, and copies of the data (cf. also Art. 15 GDPR);
- to correct or complete incorrect or incomplete data (cf. also Art. 16 GDPR);
- to the immediate deletion of data concerning them (cf. also Art. 17 DSGVO), or, alternatively, if further processing is necessary as stipulated in Art. 17 Para. 3 GDPR, to restrict said processing per Art. 18 GDPR;
- to receive copies of the data concerning them and/or provided by them and to have the same transmitted to other providers/controllers (cf. also Art. 20 GDPR);
- to file complaints with the supervisory authority if they believe that data concerning them is being processed by the controller in breach of data protection provisions (see also Art. 77 GDPR).

In addition, the controller is obliged to inform all recipients to whom it discloses data of any such corrections, deletions, or restrictions placed on processing the same per Art. 16, 17 Para. 1, 18 GDPR. However, this obligation does not apply if such notification is impossible or involves a disproportionate effort. Nevertheless, users have a right to information about these recipients.
Likewise, under Art. 21 GDPR, users and data subjects have the right to object to the controller's future processing of their data pursuant to Art. 6 Para. 1 lit. f) GDPR. In particular, an objection to data processing for the purpose of direct advertising is permissible.

## III. Information about the data processing

Your data processed when using our website will be deleted or blocked as soon as the purpose for its storage ceases to apply, provided the deletion of the same is not in breach of any statutory storage obligations or unless otherwise stipulated below.

### Server data

For technical reasons, the following data sent by your internet browser to us or to our server provider will be collected, especially to ensure a secure and stable website: These server log files record the type and version of your browser, operating system, the website from which you came (referrer URL), the webpages on our site visited, the date and time of your visit, as well as the IP address from which you visited our site.
The data thus collected will be temporarily stored, but not in association with any other of your data. \
The basis for this storage is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in the improvement, stability, functionality, and security of our website.
The data will be deleted within no more than seven days, unless continued storage is required for evidentiary purposes. In which case, all or part of the data will be excluded from deletion until the investigation of the relevant incident is finally resolved.

### Order processing

The data you submit when ordering goods and/or services from us will have to be processed in order to fulfill your order. Please note that orders cannot be processed without providing this data.
The legal basis for this processing is Art. 6 Para. 1 lit. b) GDPR.
After your order has been completed, your personal data will be deleted, but only after the retention periods required by tax and commercial law.
In order to process your order, we will share your data with the shipping company responsible for delivery to the extent required to deliver your order and/or with the payment service provider to the extent required to process your payment.
The legal basis for the transfer of this data is Art. 6 Para. 1 lit. b) GDPR.

### Customer account/registration

If you create a customer account with us via our website, we will use the data you entered during registration (e.g. your name, your address, or your email address) exclusively for services leading up to your potential placement of an order or entering some other contractual relationship with us, to fulfill such orders or contracts, and to provide customer care (e.g. to provide you with an overview of your previous orders or to be able to offer you a wishlist function). We also store your IP address and the date and time of your registration. This data will not be transferred to third parties.
During the registration process, your consent will be obtained for this processing of your data, with reference made to this privacy policy. The data collected by us will be used exclusively to provide your customer account.
If you give your consent to this processing, Art. 6 Para. 1 lit. a) GDPR is the legal basis for this processing.
If the opening of the customer account is also intended to lead to the initiation of a contractual relationship with us or to fulfill an existing contract with us, the legal basis for this processing is also Art. 6 Para. 1 lit. b) GDPR.
You may revoke your prior consent to the processing of your personal data at any time under Art. 7 Para. 3 GDPR with future effect. All you have to do is inform us that you are revoking your consent.
The data previously collected will then be deleted as soon as processing is no longer necessary. However, we must observe any retention periods required under tax and commercial law.

## Payment option: Klarna

To process orders through our online shop, we use the payment service of Klarna Bank AB, Sveavägen 46, 111 34 Stockholm, Sweden, hereinafter referred to as "Klarna", on our website.
For this purpose, we have integrated Klarna's check-out into the final order page of our online shop.
The legal basis is the fulfilment of the contract according to Art. 6 Para. 1 lit. b.)  EU General Data Protection Regulation (GDPR). In addition, we have a legitimate interest in offering effective and secure payment options, so that another legal basis ensues from Art. 6 para. 1 lit f.) GDPR.
By integrating Klarna, your internet browser loads the check-out page from a Klarna server. This means that the operating system you are using, type and version of your Internet browser, website from which the check-out has been requested, date and time of the call and the IP address are sent to Klarna - even without your interaction with the check-out page.
As soon as you complete the order in our online shop, the data you have entered in the input fields of the check-out page will be processed by Klarna at your own responsibility in order to process the payment. \
With the offered payment methods "PayPal" and "Advance Payment", processing without your further consent is limited to the transfer of the payment data to us or PayPal.
With the offered payment methods of "Purchase on Account", "Hire Purchase", "Credit Card", "Direct Debit" or "Immediate Payment", the following personal data is processed by Klarna for the purpose of payment processing and for identity and credit checking:

- Contact information such as names, addresses, date of birth, gender, email address, telephone number, mobile phone number, IP address, etc.
- Information on the processing of the order, such as product type, product number, price, etc.
- Payment information, such as debit and credit card data (card number, expiry date and CCV code), invoice data, account number, etc.

If you choose the payment method "Purchase on Account" or "Hire Purchase", Klarna collects and uses personal data and information about your previous payment behaviour to decide whether you will be granted the desired payment method. In addition, probability values for your future payment behaviour (so-called scoring) are used. Scoring is calculated on the basis of scientifically recognized mathematical and statistical methods.
At [https://cdn.klarna.com/1.0/shared/content/policy/data/de_de/data_protection.pdf](https://cdn.klarna.com/1.0/shared/content/policy/data/de_de/data_protection.pdf) Klarna provides further information on the processing described above as well as the applicable data protection regulations.

Example Data Protection Statement for Kraut und Rüben

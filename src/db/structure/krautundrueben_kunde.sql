
/***        kunde table        ***/

CREATE TABLE KUNDE (
    KUNDENNR        INTEGER NOT NULL,
    NACHNAME        VARCHAR(50),
    VORNAME         VARCHAR(50),
    GEBURTSDATUM    DATE,
    STRASSE         VARCHAR(50),
    HAUSNR          VARCHAR(6),
    PLZ             VARCHAR(5),
    ORT             VARCHAR(50),
    TELEFON         VARCHAR(25),
    EMAIL           VARCHAR(50)
);

-- primary keys

ALTER TABLE KUNDE ADD PRIMARY KEY (KUNDENNR);

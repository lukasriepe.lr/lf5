
-- init server
SET character_set_server = 'utf8';
SET collation_server = 'utf8_general_ci';

-- init connection
SET NAMES 'utf8' COLLATE 'utf8_general_ci';

/******************************************************************************/
/***                         Database krautundrueben                        ***/
/******************************************************************************/

DROP DATABASE IF EXISTS krautundrueben;
CREATE DATABASE IF NOT EXISTS krautundrueben;

USE krautundrueben;

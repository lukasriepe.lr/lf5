
/***        ernaehrungskategorie table        ***/

CREATE TABLE ERNAEHRUNGSKATEGORIE(
    ERNAEHRUNGSKATEGORIENR  INTEGER NOT NULL,
    `NAME`                  VARCHAR(50) NOT NULL,
    BESCHREIBUNG            VARCHAR(4096)
);

CREATE TABLE REZEPTERNAEHRUNGSKATEGORIE(
    REZEPTNR                INTEGER NOT NULL,
    ERNAEHRUNGSKATEGORIENR  INTEGER NOT NULL
);

-- primary keys

ALTER TABLE ERNAEHRUNGSKATEGORIE ADD PRIMARY KEY (ERNAEHRUNGSKATEGORIENR);
ALTER TABLE REZEPTERNAEHRUNGSKATEGORIE ADD PRIMARY KEY (REZEPTNR, ERNAEHRUNGSKATEGORIENR);

-- foreign keys

ALTER TABLE REZEPTERNAEHRUNGSKATEGORIE ADD FOREIGN KEY (REZEPTNR) REFERENCES REZEPT (REZEPTNR);
ALTER TABLE REZEPTERNAEHRUNGSKATEGORIE ADD FOREIGN KEY (ERNAEHRUNGSKATEGORIENR) REFERENCES ERNAEHRUNGSKATEGORIE (ERNAEHRUNGSKATEGORIENR);

-- creating new user
CREATE USER IF NOT EXISTS 'dbu_kur'@'localhost' IDENTIFIED BY '4321';
CREATE USER IF NOT EXISTS 'dbu_kur'@'%' IDENTIFIED BY '4321';

-- giving dbu_kur on localhost all permissions

GRANT ALL PRIVILEGES ON krautundrueben.* to 'dbu_kur'@'localhost';

-- giving dbu_kur on anyhost all permissions except on LIEFERANT and KUNDE

GRANT SELECT, UPDATE, DELETE, INSERT, REFERENCE ON krautundrueben.ERNAEHRUNGSKATEGORIE TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.REZEPTZUTAT TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.ZUTAT TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.REZEPTALLERGEN TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.REZEPTERNAEHRUNGSKATEGORIE TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.ALLERGEN TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.REZEPT TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.BESTELLUNG TO 'dbu_kur'@'%';
GRANT SELECT, UPDATE, DELETE, INSERT ON krautundrueben.BESTELLUNGZUTAT TO 'dbu_kur'@'%';
GRANT SELECT (LIEFERANTENNR) ON krautundrueben.LIEFERANT TO 'dbu_kur'@'%';
GRANT SELECT (KUNDENNR) ON krautundrueben.KUNDE TO 'dbu_kur'@'%';

FLUSH PRIVILEGES;


/***        ernährungskategorie        ***/

INSERT INTO ERNAEHRUNGSKATEGORIE VALUES(1,'Vegan',          'Veganismus ist eine aus dem Vegetarismus hervorgegangene Einstellung sowie Lebens- und Ernährungsweise. Vegan lebende Menschen meiden entweder alle Nahrungsmittel tierischen Ursprungs oder sie lehnen generell die Verwertung tierischer Produkte und Ausbeutung der Tiere ab.');
INSERT INTO ERNAEHRUNGSKATEGORIE VALUES(2,'Vegetarisch',    'Vegetarismus bezeichnet eine Ernährungs- und Lebensweise, welche Nahrungsmittel meidet, die von getöteten Tieren stammen. Dies sind Fleisch, Fisch (einschließlich anderer aquatischer Tiere) sowie daraus hergestellte Produkte.');
INSERT INTO ERNAEHRUNGSKATEGORIE VALUES(3,'Frutarisch',     'Frutarier, auch Fruitarier, Fructarier, Frutaner, Fruitaner oder Fruganer genannt, sind Menschen, die eine vegetarische Ernährungsweise auf der Basis von Früchten befolgen. Diese Ernährungsweise wird als Fruitarismus oder Fruganismus bezeichnet.');
INSERT INTO ERNAEHRUNGSKATEGORIE VALUES(4,'ohne Gentechnik','Die Begriffe gentechnikfrei, ohne Gentechnik oder genfrei werden verwendet, um Produkte zu bezeichnen, die frei von gentechnisch veränderten Organismen sind und auch ohne deren Hilfe erzeugt wurden.');
INSERT INTO ERNAEHRUNGSKATEGORIE VALUES(5,'Low Carb',       'Der Begriff Kohlenhydratminimierung bzw. Low-Carb bezeichnet verschiedene Ernährungsformen oder Diäten, bei denen der Anteil der Kohlenhydrate an der täglichen Nahrung reduziert wird.');

INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(200, 1);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(200, 2);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(200, 4);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(201, 1);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(201, 2);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(201, 3);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(201, 4);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(202, 1);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(202, 2);
INSERT INTO REZEPTERNAEHRUNGSKATEGORIE VALUES(202, 4);

/***        rezept        ***/

INSERT INTO REZEPT(REZEPTNR, `NAME`, ANLEITUNG) VALUES(200, 'Kartoffelpuffer', 'Kartoffeln schälen, reiben und für ca. 20 min Wasser ziehen lassen. Das überschüssige Wasser weggießen. Die geschälte und geriebene Zwiebel mit den restlichen Zutaten zu den Kartoffeln geben und alles gut verrühren. Eine Pfanne erhitzen und die Kartoffelpuffer ausbacken/braten. Um die Kartoffelpuffer in einer normalen Größe (ca. 7-9cm) zu portionieren, benutze ich für einen Kartoffelpuffer eine kleine Suppenkelle. Zu den Kartoffelpuffern essen wir immer gerne Apfelmus. Guten Appetit!');
INSERT INTO REZEPT(REZEPTNR, `NAME`, ANLEITUNG) VALUES(201, 'Apfelmus', 'Äpfel schälen, entkernen und in kleine Stückchen schneiden. Alles in einen Topf geben und zusammen mit den restlichen Zutaten 15 - 20 min auf kleiner Flamme köcheln lassen. Nun alles mit dem Pürierstab zu einem Mus pürieren. Abkühlen lassen. Fertig. Guten Appetit! Wenn man die Menge nicht nach dem Abkühlen serviert, sondern haltbar machen möchte, das Apfelmus sofort in heiß ausgespülte Gläser füllen und diese gut verschließen!');
INSERT INTO REZEPT(REZEPTNR, `NAME`, ANLEITUNG) VALUES(202, 'Tomaten-Couscous', 'Couscous in einer Schüssel mit 300 ml kochendem Salzwasser übergießen und zugedeckt 10 Minuten quellen lassen. Zwiebel halbieren und in Streifen schneiden. Knoblauch in feine Scheiben schneiden. Tomaten halbieren. Öl in einer Pfanne erhitzen. Zwiebel und Knoblauch darin bei mittlerer Hitze glasig dünsten. Tomatenmark einrühren und kurz mitdünsten. Tomaten zugeben, mit Salz, Pfeffer und 1 Prise Zucker würzen und bei mittlerer Hitze 5 Min. schmoren. Petersilien- und Minzblätter von den Stielen zupfen und hacken. Feta zerbröseln. Couscous mit einer Gabel auflockern. Mit der Tomatenmischung anrichten und mit Feta, Petersilie und Minze bestreut servieren. Nach Belieben mit 1 EL Öl beträufeln und pfeffern.');

INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(200, 1006, 10);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(200, 1002, 1);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(201, 2001, 15);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(202, 1002, 1);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(202, 1003, 6);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(202, 1009, 4);
INSERT INTO REZEPTZUTAT(REZEPTNR, ZUTATENNR, MENGE) VALUES(202, 6408, 15);

#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPT_DIR
# Files
DATABASE_FILE="${SCRIPT_DIR}/build/database.sql"
DATABASE_STRUCTURE_FILE="${SCRIPT_DIR}/build/database_structure.sql"
DATABASE_DATA_FILE="${SCRIPT_DIR}/build/database_data.sql"

## database structure
cd "${SCRIPT_DIR}/src/db/structure"

cat ./krautundrueben.sql  > $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_kunde.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_lieferant.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_zutat.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_bestellung.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_rezept.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_ernaehrungskategorie.sql  >> $DATABASE_STRUCTURE_FILE
cat ./krautundrueben_allergen.sql  >> $DATABASE_STRUCTURE_FILE

## database data
cd "${SCRIPT_DIR}/src/db/data"

cat ./krautundrueben.sql > $DATABASE_DATA_FILE
cat ./krautundrueben_kunde.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_lieferant.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_zutat.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_bestellung.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_rezept.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_ernaehrungskategorie.sql  >> $DATABASE_DATA_FILE
cat ./krautundrueben_allergen.sql  >> $DATABASE_DATA_FILE


## full database
cat ${DATABASE_STRUCTURE_FILE} ${DATABASE_DATA_FILE} > $DATABASE_FILE
